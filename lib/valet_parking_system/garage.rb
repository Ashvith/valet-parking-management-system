# frozen_string_literal: true

module ValetParkingSystem
  # Garage abstraction for the valet parking management system
  class Garage
    attr_reader :small_spot, :medium_spot, :large_spot, :parking_spots

    VALID_SIZES = %i[small medium large].freeze
    def initialize(small_spot = 0, medium_spot = 0, large_spot = 0)
      @small_spot = small_spot
      @medium_spot = medium_spot
      @large_spot = large_spot
      @parking_spots = Array.new(small_spot) { ParkingSpot.new(:small) } +
                       Array.new(medium_spot) { ParkingSpot.new(:medium) } +
                       Array.new(large_spot) { ParkingSpot.new(:large) }
    end

    def admit_the_car(license_plate_number, car_size)
      if VALID_SIZES.include? car_size
        shuffle_parking_spot
        park_vehicle(license_plate_number, car_size)
      else
        raise StandardError, "Invalid size :#{car_size} - use :small, :medium or :large"
      end
    rescue StandardError => e
      "FAILURE: #{e}"
    end

    def exit_the_car(license_plate_number)
      if license_plate_exists?(license_plate_number)
        remove_vehicle(license_plate_number)
        "SUCCESS: Car \"#{license_plate_number}\" exited successfully"
      else
        raise StandardError, "Car \"#{license_plate_number}\" is not available in the garage"
      end
    rescue StandardError => e
      "FAILURE: #{e}"
    end

    private

    def park_vehicle(license_plate_number, car_size)
      if license_plate_exists?(license_plate_number)
        raise StandardError,
              "Car \"#{license_plate_number}\" already exists"
      end

      target = allowed_spots(car_size)
      if target.count.positive?
        target.first.allot_vehicle(license_plate_number, car_size)
        "SUCCESS: Car \"#{license_plate_number}\" parked successfully"
      else
        raise StandardError, "Car \"#{license_plate_number}\" not parked - Garage capacity exceeded"
      end
    end

    def allowed_spots(car_size)
      case car_size
      when :small
        small_empty_spots | medium_empty_spots | large_empty_spots
      when :medium
        medium_empty_spots | large_empty_spots
      when :large
        large_empty_spots
      end
    end

    def empty_spots
      @parking_spots.filter(&:empty?)
    end

    def occupied_spots
      @parking_spots.reject(&:empty?)
    end

    def large_empty_spots
      empty_spots.filter(&:large?)
    end

    def medium_empty_spots
      empty_spots.filter(&:medium?)
    end

    def small_empty_spots
      empty_spots.filter(&:small?)
    end

    def large_occupied_spots
      occupied_spots.filter(&:large?)
    end

    def medium_occupied_spots
      occupied_spots.filter(&:medium?)
    end

    def small_occupied_spots
      occupied_spots.filter(&:small?)
    end

    def license_plate_exists?(license_plate_number)
      !!find_license_plate(license_plate_number)
    end

    def find_license_plate(license_plate_number)
      occupied_spots.find { |spot| spot.vehicle.license_plate_number == license_plate_number }
    end

    def remove_vehicle(license_plate_number)
      target_spot = find_license_plate(license_plate_number)
      target_spot.deallot_vehicle
    end

    def shuffle_parking_spot
      move_from_spot(:small_empty_spots, :medium_occupied_spots, [:small])
      move_from_spot(:small_empty_spots, :large_occupied_spots, [:small])
      move_from_spot(:medium_empty_spots, :large_occupied_spots, %i[small medium])
    end

    def move_from_spot(empty_spots, occupied_spots, size)
      while send(empty_spots).any? && send(occupied_spots).any? { |spot| size.include? spot.vehicle.car_size }
        target = send(occupied_spots).find do |spot|
          size.include? spot.vehicle.car_size
        end
        send(empty_spots).first.allot_vehicle(target.vehicle.license_plate_number, target.vehicle.car_size)
        target.deallot_vehicle
      end
    end
  end
end
