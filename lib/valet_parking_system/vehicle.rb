# frozen_string_literal: true

module ValetParkingSystem
  # Vehicle abstraction for the valet parking management system
  class Vehicle
    attr_reader :license_plate_number, :car_size

    def initialize(license_plate_number, car_size)
      @license_plate_number = license_plate_number
      @car_size = car_size
    end
  end
end
