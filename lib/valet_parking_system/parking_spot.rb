# frozen_string_literal: true

module ValetParkingSystem
  # Parking spot entities for the Garage object
  class ParkingSpot
    attr_reader :size, :vehicle
    attr_accessor :status

    def initialize(size)
      @size = size
      @status = :empty
      @vehicle = nil
    end

    def empty?
      @status == :empty
    end

    def large?
      @size == :large
    end

    def medium?
      @size == :medium
    end

    def small?
      @size == :small
    end

    def allot_vehicle(license_plate_number, car_size)
      @status = :occupied
      @vehicle = Vehicle.new(license_plate_number, car_size)
    end

    def deallot_vehicle
      @status = :empty
      @vehicle = nil
    end
  end
end
