# frozen_string_literal: true

require './lib/valet_parking_system/garage'
require './lib/valet_parking_system/parking_spot'
require './lib/valet_parking_system/vehicle'

# Valet Parking System module for the parking system
module ValetParkingSystem
end
