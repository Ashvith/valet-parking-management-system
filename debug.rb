# frozen_string_literal:true

require './lib/valet_parking_system'
require 'securerandom'
require 'pry'

garage = ValetParkingSystem::Garage.new(3, 2, 2)
7.times { garage.admit_the_car(SecureRandom.base64, :small) }
pp garage
binding.pry
