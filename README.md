# Valet parking management system

## Instructions
- Ensure Ruby version is `3.1.1`
- Run `bundle install` to install all the required gem files.
- Update and run `debug.rb` to test module `ValetParkingSystem`.
    - Enter `garage` to view contents of object `garage`
    - Enter `garage.admit_the_car("LICENSE_PLATE", :size)` to add vehicle to object `garage`.
    - Enter `garage.exit_the_car("LICENSE_PLATE")` to remove vehicle from object `garage`.

## Functional requirement of the system:
1. The parking garage has three types of parking spots for three kinds of vehicle sizes.

	Vehicle size: **small**, **medium**, and **large**.
	a. A small vehicle can park in a **small**, **medium**, and **large** spot.
	b. A medium vehicle can park in a medium and large spot.
	c. A large vehicle can only park in a large spot.

2. Numbers of small parking spots are X, medium are "Y" and Large are "Z" ; where X, Y, Z should be provided while initializing the code.


## Valet parking system provides below two methods for the valet:
```
def admit_the_car (license_plate_number, car_size) {
// this method checks if there is space available to park the car inside
// this method returns success if there is a parking spot available for given // carsize.
// This method also assign the car to that the specific parking spot
// This method returns failure if there is no parking spot available to park the car.
end

def exit_the_car(license_plate_number)
//return the success if the car is parked inside the car and free up space.
// return the failure if the car is not parked inside the garage
end
```

## Expectation:
1. Design a class and expose methods to "admit" and "exit" the car.
2. Variable, method, and class name should be meaningful.
3. Code should be time and space-optimized.
4. There should not be duplicate code. If you have to write the same code multiple times, then think about putting that into the shared method.
5. Read the example carefully and handle it in the code.

## Example:
1. Suppose there are 1 small, 1 medium, 1 large type of spot in the garage. Let's name the space as 1S (small), 1M, 1L.
2. Small car "A" comes in:
	a. `admit_the_car` will return success.
	b. Car A is parked into 1S.
3. Small car "B" comes in:
	a. `admit_the_car` will return success.
	b. Car B is parked into 1M.
4. Small car "C" comes in :
	a. `admit_the_car` will return success.
	b. Car C is parked into 1L.
5. Small car "A" leaves:
	a. `exit_the_car` will return success.
	b. 1S is now available.
6. Large car "D" comes in:
	a. `admit_the_car` will return success.
		i. 1M and 1L is occupied by small car but 1S is free.
		ii. So the system has to manage to shuffle the car so that small car "C" goes into parking spot 1S.
		iii. After shuffling parking spot "1L" is available for car `D`.
